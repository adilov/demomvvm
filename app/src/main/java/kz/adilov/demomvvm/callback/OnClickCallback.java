package kz.adilov.demomvvm.callback;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import kz.adilov.demomvvm.service.model.Article;
import kz.adilov.demomvvm.ui.detail.NewsDetailActivity;

public class OnClickCallback {
    public void onClick(View view, Article article) {
        Context context = view.getContext();
        Intent i = new Intent(context, NewsDetailActivity.class);
        i.putExtra("url", article.getUrl());
        context.startActivity(i);
    }
}
