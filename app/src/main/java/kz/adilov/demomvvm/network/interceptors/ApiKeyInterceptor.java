package kz.adilov.demomvvm.network.interceptors;

import android.support.annotation.NonNull;

import java.io.IOException;

import kz.adilov.demomvvm.BuildConfig;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Yes Adilov on 4/5/2017.
 */

public class ApiKeyInterceptor implements Interceptor {


    public ApiKeyInterceptor() {

    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request original = chain.request();
        HttpUrl originalHttpUrl = original.url();

        HttpUrl url = originalHttpUrl.newBuilder()
                .addQueryParameter("apiKey", BuildConfig.API_KEY)
                .build();

        Request request = original.newBuilder()
                .url(url).build();
        return chain.proceed(request);
    }

}