package kz.adilov.demomvvm.network;

import java.util.concurrent.TimeUnit;

import kz.adilov.demomvvm.BuildConfig;
import kz.adilov.demomvvm.network.interceptors.ApiKeyInterceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Yes Adilov on 02.02.17.
 */
public class RetrofitServiceGenerator {

    private static final int CONNECTION_TIMEOUT_IN_MS = 60000;
    private static final int READ_TIMEOUT_IN_MS = 60000;

    private static RetrofitServiceGenerator instance = null;


    private OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();

    private OkHttpClient okHttpClient;
    private Retrofit retrofit;

    public static RetrofitServiceGenerator getInstance() {
        if (instance == null) {
            instance = new RetrofitServiceGenerator();
        }
        return instance;
    }

    private RetrofitServiceGenerator() {

    }

    public <S> S createService(Class<S> serviceClass) {
        if (retrofit == null) {
            retrofit = getRetrofitBuilder().client(getOkHttpClient()).build();
        }

        return retrofit.create(serviceClass);
    }

    private Retrofit.Builder getRetrofitBuilder() {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.URL_BASE)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(GsonProvider.gson));
    }

    public OkHttpClient getOkHttpClient() {
        if (okHttpClient == null) {
            okHttpClientBuilder
                    .addInterceptor(new ApiKeyInterceptor())
                    .connectTimeout(CONNECTION_TIMEOUT_IN_MS, TimeUnit.MILLISECONDS)
                    .readTimeout(READ_TIMEOUT_IN_MS, TimeUnit.MILLISECONDS);

            if (BuildConfig.IS_DEBUG) {
                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                okHttpClientBuilder.addInterceptor(logging);

            }

            okHttpClient = okHttpClientBuilder.build();
        }
        return okHttpClient;
    }

}