package kz.adilov.demomvvm.network;

import io.reactivex.Observable;
import kz.adilov.demomvvm.service.model.News;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Yes Adilov on 02.02.17.
 */

public interface Api {
    @GET("top-headlines")
    Observable<News> getNews(@Query("q") String category);
}