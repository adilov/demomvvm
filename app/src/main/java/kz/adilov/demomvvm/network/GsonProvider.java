package kz.adilov.demomvvm.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by Yes Adilov on 24.11.16.
 */

public class GsonProvider {
    public static final String DATE = "yyyy-MM-dd HH:mm:ss";

    public static Gson gson = new GsonBuilder()
            .setDateFormat(DATE)
//            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create();

}