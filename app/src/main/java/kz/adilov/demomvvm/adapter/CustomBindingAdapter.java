package kz.adilov.demomvvm.adapter;

import android.databinding.BindingAdapter;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import kz.adilov.demomvvm.utility.DateUtils;

public class CustomBindingAdapter {
    @BindingAdapter("visibleGone")
    public static void showHide(View view, boolean show) {
        view.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter("dateText")
    public static void convertToDate(TextView view, String date) {
        view.setText(DateUtils.Companion.convertToDateString(date));
    }


    @BindingAdapter("loadurl")
    public static void loadurl(WebView mWebview, String url) {
        mWebview.getSettings().setJavaScriptEnabled(true); // enable javascript
        mWebview.loadUrl(url);
    }

}