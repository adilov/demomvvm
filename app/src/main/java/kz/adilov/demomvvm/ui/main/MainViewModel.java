package kz.adilov.demomvvm.ui.main;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;

import kz.adilov.demomvvm.service.model.News;
import kz.adilov.demomvvm.service.repository.NewsRepository;

public class MainViewModel extends AndroidViewModel {
    private final LiveData<News> newsLiveData;

    public ObservableField<News> news = new ObservableField<>();

    public MainViewModel(@NonNull Application application) {
        super(application);
        newsLiveData = NewsRepository.getInstance().getNews("bitcoin");
    }

    public LiveData<News> getObservableNews() {
        return newsLiveData;
    }

    public void setNews(News news) {
        this.news.set(news);

    }
}

